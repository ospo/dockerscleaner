#!/bin/bash
#dockersCleaner
#Cleaner docker by default tools, but run if locks container run only.
#v0.2

#Config section
readonly vf_locksContainers=("redmine" "mariadb")
readonly vf_minDiskSpaceMb=3000
readonly vf_partitionDevice=/dev/sda3
#End config section

v_scriptNameExt=$(basename "$0")
readonly vf_scriptName=${v_scriptNameExt%.*}
unset v_scriptNameExt
declare v_force=0
declare v_diskAvail=0
printhelp() {
	cat << ENDHELP
Usage: $vf_scriptName key
Keys:
    -f, --force                             run even if disk space enough
ENDHELP
}

#ret.code
#0 - disk space enough
#1 - not enough
checkDisk() {
	v_diskAvail=$(df -m --output=avail "$vf_partitionDevice" | tail -n 1 | xargs)
	if (( v_diskAvail < vf_minDiskSpaceMb )); then
		return 1
	fi
}

if [ "$1" == "--help" ]; then
	printhelp;
	exit 0;
fi

for arg in "$@"; do
	shift
	case "$arg" in
		"--force") set -- "$@" "-f" ;;
	*) 	if echo "$arg" | grep -Eq "^-\w$"; then
			set -- "$@" "$arg"
		else
			echo "$vf_scriptName: wrong key \"$arg\""
			echo "Try \"$vf_scriptName --help\""
			exit 3
		fi;;
	esac
done

while getopts "f" opt
do
	case $opt in
		f) v_force=1;;
		*) exit 2;;  #Message printing by getopts function
	esac
done

if (( v_force == 0 )); then
	if ! checkDisk; then
		logger "Disk overflow ($v_diskAvail < $vf_minDiskSpaceMb). Starting docker files cleaning." -p local0.warning -e -t "$vf_scriptName"
	else
		exit 0
	fi
else
	logger "Forcing docker files cleaning" -p local0.info -e -t "$vf_scriptName"
fi

exec > >(tee >(logger -p local0.notice -t "$vf_scriptName"))
exec 2> >(logger -p local0.err -e -t "$vf_scriptName" -s)

declare v_result
for v_curContainer in "${vf_locksContainers[@]}"; do
	v_result=$(docker container inspect -f "{{.State.Running}}" "$v_curContainer")
	if (( $? != 0 )); then
		logger "Err during execute command. Terminate." -e -p local0.err -t "$vf_scriptName"
		exit 2
	fi

	if [ "$v_result" != "true" ]; then
		logger "Container \"$v_curContainer\" is no running. Terminate." -t "$vf_scriptName" -p local0.err
		exit 1
	fi
done

logger "docker system prune: $(docker system prune -a -f)" -p local0.notice -t "$vf_scriptName"
